<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/departamentos', 'DepartamentoController@index')->name('departamentos');
Route::get('/produtos', 'ProdutosController@index')->name('produtos');
Route::get('/representantes', 'RepresentanteController@index')->name('representantes');
Route::get('/saldo-flex', 'SaldoFLexController@index')->name('saldo-flex');

Route::get('btn-importar', 'ProdutosController@importarProdutos');
Route::get('btn-importar-rep', 'RepresentanteController@importar');
Route::get('btn-importar-deptos', 'DepartamentoController@importarDepartamentos');

Route::get('/pesquisa-produto', 'ProdutosController@searchProduto');
Route::get('/alterar-produto/{id}', 'ProdutosController@edit');
Route::post('/update-produto/{id}', 'ProdutosController@updateProduto');

Route::get('/pesquisa-saldo', 'SaldoFLexController@searchSaldo');
Route::get('/add-flex', 'SaldoFLexController@create');
Route::post('/novoFlex', 'SaldoFLexController@store');

Route::post('/update-depto/{id}', 'DepartamentoController@update');
Route::get('/alterar-depto/{id}', 'DepartamentoController@edit');
Route::get('/pesquisa-depto', 'DepartamentoController@searchDepto');
