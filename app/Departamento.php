<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'departamento';

    protected $fillable = [
        'id', 'nome', 'desconto_maximo', 'acrescimo_maximo'
    ];
}
