<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representante extends Model
{
    protected $table = 'vendedores';

    protected $fillable = [
        'id', 'nome', 'email'
    ];
}
