<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $fillable = [
        'id', 'nome', 'preco_imposto', 'peso_unidade'
    ];
}
