<?php

namespace App\Http\Controllers;

use App\Representante;
use Illuminate\Http\Request;
use App\SaldoFLex;
use Illuminate\Support\Str;

class SaldoFLexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $saldos = SaldoFlex::join('vendedores', 'saldo_flex.id_vendedor', '=', 'vendedores.id')->select('saldo_flex.*', 'vendedores.nome')->orderBy('saldo_flex.id', 'DESC')->paginate(15);
        $reps = Representante::orderBy('nome')->get();

        return view('flex_list')->with(compact('saldos'))->with(compact('reps'));
    }

    public function create()
    {
        $reps = Representante::orderBy('nome')->get();
        return view('saldo-flex-form')->with(compact('reps'));
    }

    public function store(Request $request)
    {
        $saldoAntigo = SaldoFLex::where('id_vendedor', $request->idRepresentante)->orderBy('id', 'DESC')->first();

        //cria a descrição
        if ($request->tipo == 'C') {
            $descricao = 'Lançamento de crédito em conta';
        } else {
            $descricao = 'Lançamento de débito em conta';
        }

        //formata o lançamento para o valor padrão do db (ponto no lugar da vírgula)
        $lancamento = Str::replaceArray('.', [''], $request->lancamento);
        $lancamento = Str::replaceArray(',', ['.'], $lancamento);

        //cria a instancia do model com os campos padrão
        $saldo = new SaldoFLex();
        $saldo->id_vendedor = $request->idRepresentante;
        $saldo->descricao = $descricao;
        $saldo->tipo = $request->tipo;
        $saldo->lancamento = $lancamento;

        //checa se existe saldo anterior
        if ($saldoAntigo) {

            //soma ou subtrai o valor do saldo atual
            if ($request->tipo == 'C') {
                $saldo->saldo = $saldoAntigo->saldo + $lancamento;
            } else {
                $saldo->saldo = $saldoAntigo->saldo - $lancamento;
            }

            $saldo->saldo_anterior = $saldoAntigo->saldo;
        } else {
            $saldo->saldo = $lancamento;
            $saldo->saldo_anterior = 0.00;
        }
        $saldo->save();

        return redirect()->back()->with('alert', 'Lançamento cadastrado com sucesso!');
    }

    public function searchSaldo(Request $request)
    {
        $search = $request->get('query');
        $saldos = SaldoFlex::join('vendedores', 'saldo_flex.id_vendedor', '=', 'vendedores.id')->select('saldo_flex.*', 'vendedores.nome')->where('saldo_flex.id_vendedor', $search)->orderBy('saldo_flex.id', 'DESC')->paginate(15);
        $reps = Representante::orderBy('nome')->get();

        return view('flex_list')->with(compact('saldos'))->with(compact('reps'));
    }
    //Função sem utilização
    public function delete($id)
    {
        SaldoFLex::destroy($id);

        $saldos = SaldoFlex::join('vendedores', 'saldo_flex.id_vendedor', '=', 'vendedores.id')->select('saldo_flex.*', 'vendedores.nome')->orderBy('saldo_flex.id', 'DESC')->paginate(15);
        $reps = Representante::orderBy('nome')->get();

        return view('flex_list')->with('alert', 'Lançamento apagado com sucesso!')->with(compact('saldos'))->with(compact('reps'));
    }
}
