<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class ProdutosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $produtos = Produto::orderBy('nome')->paginate(15);

        return view('produtos', compact('produtos'));
    }

    public function importarProdutos()
    {
        $response = Http::get('http://131.100.251.82:8086/construshow_api/public/api/produtos');
        $produtos = json_decode($response);

        foreach ($produtos as $p) {

            Produto::updateOrCreate(
                ['id' => $p->iditem],
                [
                    'nome' => $p->descricao,
                    'preco' => $p->preco
                ]
            );
        }

        $produtos = Produto::orderBy('nome')->paginate(15);

        return redirect()->back()->with('alert', 'Importação finalizada com sucesso!')->with(compact($produtos));
    }

    public function edit(Request $request)
    {
        $produto = Produto::find($request->id);

        return view('produto-form', compact('produto'));
    }

    public function updateProduto(Request $request, $id)
    {
        $produto = Produto::find($id);

        $produto->preco_imposto = Str::replaceArray('.', [''], $request->preco_imposto);
        $produto->preco_imposto = Str::replaceArray(',', ['.'], $produto->preco_imposto);

        $produto->peso_unidade = Str::replaceArray('.', [''], $request->peso_unidade);
        $produto->peso_unidade = Str::replaceArray(',', ['.'], $produto->peso_unidade);

        $produto->save();

        return redirect()->back()->with('alert', 'Produto atualizado com sucesso!');
    }

    public function searchProduto(Request $request)
    {
        $search = $request->get('q');
        $produtos = Produto::where('nome', 'LIKE', $search . '%')->orWhere('id', $search)->paginate(20);
        return view('produtos', compact('produtos'));
    }
}
