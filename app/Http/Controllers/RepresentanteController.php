<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Representante;
use Illuminate\Support\Facades\Http;

class RepresentanteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $reps = Representante::orderBy('nome')->paginate(15);

        return view('vendedores', compact('reps'));
    }


    public function importar()
    {
        $response = Http::get('http://131.100.251.82:8086/construshow_api/public/api/vendedores');
        $reps  = json_decode($response);

        foreach ($reps as $r) {

            Representante::updateOrCreate(
                ['id' => $r->idpess],
                [
                    'nome' => $r->fantasia,
                    'email' => $r->email
                ]
            );
        }

        $reps = Representante::orderBy('nome')->paginate(15);

        return redirect()->back()->with('alert', 'Importação finalizada com sucesso!')->with(compact($reps));
    }
}
