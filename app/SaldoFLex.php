<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaldoFLex extends Model
{

    use SoftDeletes;

    protected $table = 'saldo_flex';

    protected $fillable = [
        'id', 'id_vendedor', 'descricao', 'saldo', 'saldo_anterior', 'lancamento', 'tipo'
    ];

    protected $hidden = [
        'deleted_at', 'updated_at'
    ];
}
