@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-sm">

        </div>
        <div class="col-sm">
        </div>
        <div class="col-sm">
            <a href="/add-flex" class="btn btn-success btn-block p-3">
                Adicionar Lançamento
            </a>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col-md-12">
            <br />
            <div class="card">
                <div class="card-header">Saldo Flex</div>

                <div class="card-body">

                    <form action="/pesquisa-saldo" method="get">
                        <div class="col-md-8">
                            <label for="selector">Selecione o representante</label>
                        </div>
                        <div class="input-group">
                            <div class="col-md-6">

                                <select class="form-control" id="selector" name="query">
                                    @foreach($reps as $r)
                                    <option value="{{ $r->id }}">{{ $r->id . ' - ' . $r->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Filtrar pelo representante
                                </button>
                            </div>
                            <div class="col-md-3">
                                <a href="saldo-flex" class="btn btn-secondary btn-block">
                                    Mostrar todos
                                </a>
                            </div>
                            </span>
                        </div>
                    </form>

                    <br />
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Representante</th>
                                    <th scope="col">Descrição</th>
                                    <th scope="col">Lançamento</th>
                                    <th scope="col">Saldo Anterior</th>
                                    <th scope="col">Saldo Atual</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($saldos as $r)
                                <tr>
                                    <th scope="row">{{ $r->id }}</th>
                                    <td>{{ $r->id_vendedor . ' - ' . $r->nome }}</td>
                                    <td>{{ $r->descricao }}</td>
                                    <td>{{ $r->lancamento }}</td>
                                    <td>{{ $r->saldo_anterior }}</td>
                                    <td>{{ $r->saldo }}</td>
                                    <!--<td><a href="/dltFlex/{{ $r->id }}" type="button" class="btn btn-outline-danger" onclick="return confirm('Você tem certeza que deseja apagar esse registro?\n\n A ação não poderá ser desfeita.')">Apagar</a></td>-->
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mx-auto">
                    {{ $saldos->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection