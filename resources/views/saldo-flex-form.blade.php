@extends('layouts.app')

@section('content')
<div class="container">
    <form class="form-horizontal" action="/novoFlex" method="post">
        <fieldset>
            <div class="row">
                <div class="col-md-6">
                    <h1>Novo lançamento do flex</h1>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-2">
                    <a href="saldo-flex" type="button" class="btn btn-outline-primary btn-block">Voltar para a lista</a>
                </div>
            </div>
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label class="col-md-4 ">Código</label>
                <div class="col-md-2">
                    <input class="form-control" name="codigo" id="codigo" type="text" disabled>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label for="selectRepresentante">Representante</label>
                <select class="form-control" id="idRepresentante" name="idRepresentante">
                    @foreach($reps as $r)
                    <option value="{{$r->id}}">{{ $r->id . ' - ' . $r->nome }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">

                <div class="row col-sm-12">
                    <div class="col-sm-6">
                        <label for="selectTipo">Selecione o tipo</label>
                        <select class="form-control" id="selectTipo" name="tipo">
                            <option value="C">Crédito</option>
                            <option value="D">Débito</option>
                        </select>
                    </div>

                    <div class="col-sm-6">
                        <label for="inputEmail">Valor do lançamento</label>
                        <input class="form-control lancamento" id="lancamento" type="text" placeholder="0,00" name="lancamento" required>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="row col-sm-12">
                    <div class="col-sm-6">
                        <label>Descrição</label>
                        <input class="form-control descricao" id="descricao" type="text" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Saldo anterior</label>
                        <input class="form-control saldo_anterior" id="saldo_anterior" type="text" disabled>
                    </div>
                    <div class="col-sm-3">
                        <label>Saldo atual</label>
                        <input class="form-control saldo_atual" id="saldo_atual" type="text" disabled>
                    </div>
                </div>
            </div>

            <br />
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success btn-block p-3" onclick="return confirm('Cadastrar lançamento?')">Adicionar</button>

                </div>
            </div>
        </fieldset>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    $(document).ready(function($) {
        console.log('iniciou')
        $('.lancamento').mask('000.000.000,00', {
            reverse: true
        });
        $('.saldo_anterior').mask('000.000.000,00', {
            reverse: true
        });
        $('.saldo_atual').mask('000.000.000,00', {
            reverse: true
        });
    })
</script>
<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection