@extends('layouts.app')

@section('content')
<div class="container">

    <a href="btn-importar" type="button" class="btn btn-dark">Importar produtos</a>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <br />
            <div class="card">
                <div class="card-header">Produtos</div>

                <div class="card-body">

                    <form action="/pesquisa-produto" method="get">
                        <div class="input-group">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="q" placeholder="Pesquisar por código ou descrição"> <span class="input-group-btn">
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Pesquisar
                                </button>
                            </div>
                            </span>
                        </div>
                    </form>
                    <br />
                    <table class="table table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Peso da unidade</th>
                                <th scope="col">Valor do imposto</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produtos as $p)
                            <tr>
                                <th scope="row">{{ $p->id }}</th>
                                <td>{{ $p->nome }}</td>
                                <td>{{ $p->peso_unidade }}</td>
                                <td>{{ $p->preco_imposto }}</td>
                                <td><a href="/alterar-produto/{{ $p->id }}" type="button" class="btn btn-link">Alterar</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="mx-auto">
                    {{ $produtos->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var msg = "{{ Session::get('alert') }}";
    var exist = "{{ Session::has('alert') }}";

    if (exist) {
        alert(msg);
    }
</script>
@endsection