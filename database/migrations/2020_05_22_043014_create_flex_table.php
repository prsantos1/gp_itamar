<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldo_flex', function (Blueprint $table) {
            $table->id();
            $table->integer('id_vendedor');
            $table->string('descricao');
            $table->decimal('saldo', 18, 2);
            $table->decimal('saldo_anterior', 18, 2);
            $table->decimal('lancamento', 18, 2);
            $table->char('tipo', 1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldo_flex');
    }
}
